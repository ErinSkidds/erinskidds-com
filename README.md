# [ErinSkidds.com](https://erinskidds.com)

This is my online portfolio. I have all of my projects, work experience, and more listed here. This repository shows exactly how the website is coded. You can look at any of the files above and get a feel for how I code.